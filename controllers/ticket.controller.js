const Ticket = require("../models/Ticket");
const mongoose = require("mongoose")

// Listar ticket
const listTictets = async (req, res) => {
    try{
        const tickets = await Ticket.find();
       res.json(tickets)
     
     } catch (error){
        req.send(error);
    }
}

// Obtener ticket
const obtenerTicket = async (req,res) =>{
    try{
   let ticket = await Ticket.findById(req.params.id);
   if (!ticket){
       res.status(404).json({msg: 'el ticket no existe'})
   }
   res.json(ticket);
   
    }catch (error){
       console.log(error)
       res.status(500).send("hay un error al recibir los datos");
   
   }
   }




//Crear Ticket 
const addTicket = async (req, res) => {
  try {
      const newTicket = new Ticket(req.body);
      const savedTicket = await newTicket.save();
      res.json(savedTicket);
  } catch (error) {
      res.send(error);
  }
}



   // Eliminar ticket
   const eliminarTicket = async (req,res) =>{
    try{
        let ticket = await Ticket.findById(req.params.id);
        if (!ticket){
            res.status(404).json({msg: 'el ticket no existe'})
        }
        await Ticket.findByIdAndRemove({ _id: req.params.id})
        res.json({msg: 'ticket eliminado con exito'});
}catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

//Actualizar ticket1

const actualizarTicket = async (req,res) =>{
    try{
     const {fecha, cliente, asunto, areasolicitante, tipo, criticidad, estado, ultimocambio, fecharesolucion, fechacierre, responsable } = req.body;
     let ticket = await Ticket.findById(req.params.id); 
     if (!ticket){
        res.status(404).json({msg: 'el producto no existe'})
    }
    ticket.fecha = fecha;
    ticket.cliente = cliente;
    ticket.asunto = asunto;
    ticket.areasolicitante = areasolicitante;
    ticket.tipo = tipo;
    ticket.criticidad = criticidad;
    ticket.estado = estado;
    ticket.ultimocambio = ultimocambio;
    ticket.fecharesolucion = fecharesolucion;
    ticket.fechacierre = fechacierre;
    ticket.responsable = responsable;

    ticket = await Ticket.findOneAndUpdate({_id: req.params.id}, ticket, {new:true})
    res.json(ticket);

    }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}



module.exports = {
    listTictets,
    obtenerTicket,
    addTicket,
    eliminarTicket,
    actualizarTicket,
    

}


