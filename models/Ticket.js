
const mongoose = require('mongoose');

const ticketSchema = mongoose.Schema({
    fecha:{
        type: String,
        required: true
    },
    cliente:{
        type: String,
        required: true
    },
    asunto:{
        type: String,
        required: true
    },
    areasolicitante:{
        type: String,
        required: true
    },
    tipo:{
        type: String,
        required: true
    },
    criticidad:{
        type: String,
        required: true
    },
    estado:{
        type: String,
        required: true
    },
    ultimocambio:{
        type: String,
        required: true
    },
    fecharesolucion:{
        type: String,
        required: true
    },
    fechacierre:{
        type: String,
        required: true
    },
    responsable:{
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Ticket', ticketSchema );
    