const express = require('express');
const {
    listTictets,
    obtenerTicket,
    addTicket,
    eliminarTicket,
    actualizarTicket, 
    
} = require('../controllers/ticket.controller')

const router = express.Router();



router.get('/', listTictets);
router.get('/:id', obtenerTicket);
router.post('/', addTicket);
router.delete('/:id', eliminarTicket);
router.put('/:id', actualizarTicket);







module.exports = router;