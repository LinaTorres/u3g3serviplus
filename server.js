const app = require('./app');
const mongodbConnection = require('./database');
const ticketRouters = require('./routers/ticket.routers');


// Conexión a la base de datos
mongodbConnection();

// inicializamos las rutas
app.use('/tickets', ticketRouters);


app.listen(4000, () => {
    console.log('Sever is running on port 4000');
})